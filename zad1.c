#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int choice =0;
    char surname[20]="";

    struct Client *head;
    head=(struct Client*)malloc(sizeof(struct Client));
    strcpy(head->surname,"Kowalski");
    head->next = NULL;

    while (1)
    {
        while(choice<1 || choice>5)
        {
            printf("1-nowy klient\n2-usun klienta po nazwisku\n3-wyswietl ilosc klientow\n4-wyswietl nazwiska klientow\n5-zakoncz\n");
            scanf("%d",&choice);
        }

        //printf("%d\n",choice);

        switch (choice)
        {
            case 1:
                printf("podaj nazwisko nowej osoby\n");
                scanf("%s",surname);
                push_back(&head,surname);
            break;
        
            case 2:
                surname[0]='\0';
                printf("prosze podac nazwisko osoby usuwanej\n");
                scanf("%s",surname);
                pop_by_surname(&head,surname);
            break;

            case 3:
                printf("\nilosc klientow=%d\n", list_size(head));
            break;

            case 4:
                show_list(head);
            break;

            case 5:
                printf("koniec programu");
                return 0;
            break;
        default:
            break;
        }
        choice=0;
    }
    
    
}